﻿using Application.Extensions;
using Application.Helpers.Exceptions;

namespace Tests.Application.Helpers.Exceptions;

public class UnsupportedCultureExceptionTests
{
    private const string ANY_MESSAGE = "Culture is not supported.";
    
    [Fact]
    public void WhenErrorObject_ThenErrorTypeShouldBeUnsupportedCultureException()
    {
        // Arrange
        var unsupportedCultureException = new UnsupportedCultureException(ANY_MESSAGE);
        
        // Act
        var actual = unsupportedCultureException.ErrorObject();
        
        // Assert
        actual.ErrorType.ShouldBe("UnsupportedCultureException");
    }
    
    [Fact]
    public void WhenErrorObject_ThenErrorMessageShouldBeSpecifiedMessage()
    {
        // Arrange
        var unsupportedCultureException = new UnsupportedCultureException(ANY_MESSAGE);
        
        // Act
        var actual = unsupportedCultureException.ErrorObject();
        
        // Assert
        actual.ErrorMessage.ShouldBe(ANY_MESSAGE);
    }
}