﻿using Domain.Entities;
using Domain.Entities.Books;
using Domain.Entities.Identity;
using Microsoft.EntityFrameworkCore;

namespace Application.Interfaces;

public interface IGarneauTemplateDbContext
{
    DbSet<Member> Members { get; }
    DbSet<Book> Books { get; }
    DbSet<Ingredient> Ingredients { get; }
    DbSet<Recette> Recettes { get; }
    DbSet<IngredientRecette> IngredientRecettes { get; }

    Task<int> SaveChangesAsync(CancellationToken? cancellationToken = null);
}