﻿using Application.Interfaces;
using Domain.Entities;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;


namespace Infrastructure.Repositories.Ingredients;

public class IngredientRepository : IIngredientRepository
{
    private readonly IGarneauTemplateDbContext _context;

    public IngredientRepository(IGarneauTemplateDbContext context)
    {
        _context = context;
    }

    public List<Ingredient> LireTous()
    {
        return _context.Ingredients.AsNoTracking().ToList();
    }


}