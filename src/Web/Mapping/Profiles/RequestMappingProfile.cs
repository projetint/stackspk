﻿using AutoMapper;
using Domain.Common;
using Domain.Entities.Books;
using Web.Dtos;
using Web.Features.Admins.Books.CreateBook;
using Web.Features.Admins.Books.EditBook;

namespace Web.Mapping.Profiles;

public class RequestMappingProfile : Profile
{
    public RequestMappingProfile()
    {
        CreateMap<TranslatableStringDto, TranslatableString>().ReverseMap();

        CreateMap<CreateBookRequest, Book>();

        CreateMap<EditBookRequest, Book>();
    }
}