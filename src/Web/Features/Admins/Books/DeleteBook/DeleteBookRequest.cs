﻿namespace Web.Features.Admins.Books.DeleteBook;

public class DeleteBookRequest
{
    public Guid Id { get; set; }
}