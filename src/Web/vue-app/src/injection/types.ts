export const TYPES = {
  IApiService: Symbol.for("IApiService"),
  IMemberService: Symbol.for("IMemberService"),
  IBookService: Symbol.for("IBookService"),
  AxiosInstance: Symbol.for("AxiosInstance")
};
