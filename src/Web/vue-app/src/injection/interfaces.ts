// eslint-disable-next-line @typescript-eslint/no-empty-interface
import {
  ICreateBookRequest,
  IEditBookRequest,
} from "@/types/requests"
import {SucceededOrNotResponse} from "@/types/responses"
import {Book, IAuthenticatedMember, Member} from "@/types/entities"

export interface IApiService {
  headersWithJsonContentType(): any

  headersWithFormDataContentType(): any

  buildEmptyBody(): string
}

export interface IMemberService {
  getCurrentMember(): Promise<IAuthenticatedMember | undefined>
}

export interface IBookService {
  getAllBooks(): Promise<Book[]>

  getBook(bookId: string): Promise<Book>

  deleteBook(bookId: string): Promise<SucceededOrNotResponse>

  createBook(request: ICreateBookRequest): Promise<SucceededOrNotResponse>

  editBook(request: IEditBookRequest): Promise<SucceededOrNotResponse>
}