import {AxiosResponse} from "axios"
import {injectable} from "inversify"

import "@/extensions/date.extensions"
import {ApiService} from "@/services/apiService"
import {IMemberService} from "@/injection/interfaces"
import {IAuthenticatedMember} from "@/types/entities/authenticatedMember"

@injectable()
export class MemberService extends ApiService implements IMemberService {
  public async getCurrentMember(): Promise<IAuthenticatedMember | undefined> {
    try {
      const response = await this
        ._httpClient
        .get<IAuthenticatedMember, AxiosResponse<IAuthenticatedMember>>(`${process.env.VUE_APP_API_BASE_URL}/api/members/me`)
      return response.data
    } catch (error) {
      return Promise.reject(error)
    }
  }
}