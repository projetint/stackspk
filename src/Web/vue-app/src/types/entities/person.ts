export interface IPerson {
  id?: string
  crmId?: number
  firstName?: string
  lastName?: string
  jobTitle?: string
  email?: string
  phoneNumber?: string
  phoneExtension?: number
  apartment?: number
  street?: string
  city?: string
  zipCode?: string
}