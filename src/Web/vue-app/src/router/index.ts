import i18n from "@/i18n";
import {Role} from "@/types";
import {useMemberStore} from "@/stores/memberStore";
import {createRouter, createWebHistory} from "vue-router";

import Home from "../views/Home.vue";

import Admin from "../views/admin/Admin.vue";
import AdminBookIndex from "@/views/admin/AdminBookIndex.vue";
import AdminAddBookForm from "@/views/admin/AdminAddBookForm.vue";
import AdminEditBookForm from "@/views/admin/AdminEditBookForm.vue";

const router = createRouter({
  // eslint-disable-next-line
  scrollBehavior(to, from, savedPosition) {
    // always scroll to top
    return {top: 0};
  },
  history: createWebHistory(),
  routes: [
    {
      path: i18n.t("routes.home.path"),
      name: "home",
      component: Home,
    },
    {
      path: i18n.t("routes.admin.path"),
      name: "admin",
      component: Admin,
      meta: {
        requiredRole: Role.Admin,
        noLinkInBreadcrumbs: true,
      },
      children: [
        {
          path: i18n.t("routes.admin.children.books.path"),
          name: "admin.children.books",
          component: Admin,
          children: [
            {
              path: "",
              name: "admin.children.books.index",
              component: AdminBookIndex,
            },
            {
              path: i18n.t("routes.admin.children.books.add.path"),
              name: "admin.children.books.add",
              component: AdminAddBookForm,
            },
            {
              path: i18n.t("routes.admin.children.books.edit.path"),
              alias: i18n.t("routes.admin.children.books.edit.path"),
              name: "admin.children.books.edit",
              component: AdminEditBookForm,
              props: true
            },
          ],
        }
      ]
    }
  ]
});

// eslint-disable-next-line no-unused-vars
router.beforeEach(async (to, from) => {
  const memberStore = useMemberStore()
  if (!to.meta.requiredRole)
    return;
  const isRoleArray = Array.isArray(to.meta.requiredRole)
  const doesNotHaveGivenRole = !isRoleArray && !memberStore.hasRole(to.meta.requiredRole as Role);
  const hasNoRoleAmountRoleList = isRoleArray && !memberStore.hasOneOfTheseRoles(to.meta.requiredRole as Role[]);
  if (doesNotHaveGivenRole || hasNoRoleAmountRoleList) {
    return {
      name: "home",
    };
  }
});

export const Router = router;