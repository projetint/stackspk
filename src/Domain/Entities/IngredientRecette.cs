﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class IngredientRecette : AuditableAndSoftDeletableEntity
    {
        public Ingredient Ingredient { get; set; } = default!;
        public Recette Recette { get; set; } = default!;
    }
}
