﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Recette : AuditableAndSoftDeletableEntity
    {
        public string Nom { get; set; } = default!;
        public string Type { get; set; } = default!;
        public string Slug { get; set; } = default!;
        public int DureeEnMin { get; set; } = default!;
        public List<IngredientRecette> IngredientRecettes { get; set; } = default!;
    }
}
