﻿using Domain.Entities;

namespace Domain.Repositories;

public interface IMemberRepository
{
    int GetMemberCount();
    List<Member> GetAllWithUserEmail(string userEmail);
    Member FindById(Guid id);
    Member? FindByUserId(Guid userId, bool asNoTracking = true);
    Member? FindByUserEmail(string userEmail);
}